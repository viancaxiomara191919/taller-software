/**
 * @file Template.cpp
 * @author VIANCAXIOMARA191919@GMAIL.COM
 * @brief Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas y el pago por hora.
Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga un 50% mas que una hora normal.
Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.

 * @version 1.0
 * @date 21.02.2022
 * 
 */
 /*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/

 #include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float totalshorastrabajds;
float pagoporhora;
int pagofinal=0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float pagosemmanaaal( float totalshorastrabajds, float pagoporhora);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	ShowResults();
	Calculate();
	
}
//=====================================================================================================

void CollecData(){
	cout<<"*********************************************\r\n";
	cout<<"**            INSERT DATA                  **\r\n";
	cout<<"*********************************************\r\n";
    cout<<"INGRESE LA CANTIDAD DE HORAS";
    cin>>totalshorastrabajds;
    cout<<"INGRESE EL PAGO POR HORA: ";
    cin>>pagoporhora;
}
//=====================================================================================================

 void Calculate(){

 	pagofinal=pagosemmanaaal( totalshorastrabajds, pagoporhora);
 }   
   
   

//=====================================================================================================
void ShowResults(){
    cout<<"*********************************************\r\n";
	cout<<"**             how result                  **\r\n";
	cout<<"**********************************************\r\n";
	cout<<"EL PAGO FINAL ES: "<<pagofinal;
}
//=====================================================================================================
float pagosemmanaaal( float totalshorastrabajds, float pagoporhora){

   int horsextras=0;
   int pagooo=0;
   int pagofinal=0;
 if(totalshorastrabajds>40)
 {
 	horsextras=40-totalshorastrabajds;
 	pagooo=pagoporhora*40 +(0.5)*horsextras;
 }
 if(pagooo>500)
 {
 	pagofinal=(0.9)*pagooo;
 }
return 0;
}
//=====================================================================================================
