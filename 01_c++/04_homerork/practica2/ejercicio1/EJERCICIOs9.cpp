/**
 * @file Exercise9.cpp
 * @author viancaxiomara191919@gmail.com
 * @brief 9.- Calcular la comisi�n sobre las ventas totales de un empleado, sabiendo 
 que el empleado no recibe comisi�n si su venta es hasta S/.150, si la venta es superior
 a S/.150 y menor o igual a S/.400 el empleado recibe una comisi�n del 10% de las ventas
 y si las ventas son mayores a 400, entonces la comisi�n es de S/.50 m�s el 9% de las ventas.
 * @version 1.0
 * @date 23.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/


int TOTAL=0;
int venta1=150;
int venta2=400;
int comisiondeventasInput;



/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int Comisionedeventaaas(int comisiondeventasInput);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"====================================\r\n";
	cout<<"INSERTE LA COMISION: ";
	cin>>comisiondeventasInput;
}
//=====================================================================================================

void Calculate(){
    
	TOTAL=Comisionedeventaaas( comisiondeventasInput);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"====================================\r\n";
	cout<<"LA COMISION ES: "<<TOTAL;
}
//=====================================================================================================

int Comisionedeventaaas(int comisiondeventasInput){
	int TOTAL=0;
	int com=50;
	
	if (comisiondeventasInput<150){
		TOTAL=0;
	}
	if (comisiondeventasInput>150 && comisiondeventasInput<400)
	{
	     TOTAL=comisiondeventasInput*(0.1);
	
	}
 if (comisiondeventasInput>400)
	{
		TOTAL=com + comisiondeventasInput*(0.09);
	}
	return TOTAL;
}




//=====================================================================================================
