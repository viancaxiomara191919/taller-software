/**
 * @file Exercise7.cpp
 * @author viancaxiomara191919@gmail.com
 * @brief 7.- Una compa��a de alquiler de autos emite la factura de sus clientes teniendo 
 en cuenta la distancia recorrida, si la distancia no rebasa los 300 km., se cobra una tarifa
 fija de S/.250, si la distancia recorrida es mayor a 300 km. y hasta 1000 km. Se cobra la
 tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km. y si la distancia recorrida
 es mayor a 1000 km.,  la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 
 1000 a raz�n de S/. 30, y S/.20 para las distancias mayores de 1000 km. Calcular el monto
	 que pagar� un cliente.
 * @version 1.0
 * @date 23.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include "math.h"
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

int cantidad=250;
int pagodistan=0;
int distancia1=300;
int distancia2=1000;
int tarifa1=30;
int distanciaviajeInput=0;
int tarifa2=20;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
int pagodistancia(int distanciaviajeInput,int tarifa1, int tarifa2);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
*  												FUNCTION DEFINITION
*******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"====================================\r\n";
	cout<<"DISTANCIA REALIZADA: ";
	cin>>distanciaviajeInput;
}
//=====================================================================================================

void Calculate(){
	pagodistan=cantidad+pagodistan;
	pagodistan=pagodistancia( distanciaviajeInput,tarifa1,  tarifa2);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"====================================\r\n";
	cout<<"ALQUILER ES: "<<pagodistan;
}
//=====================================================================================================

int pagodistancia(int distanciaviajeInput, int tarifa1, int tarifa2){
	if (distanciaviajeInput>distancia2){
		pagodistan=tarifa2*(distanciaviajeInput-distancia2)+(distancia2-distancia1)*tarifa2+pagodistan;
	}else if (distanciaviajeInput>distancia1){
		pagodistan=cantidad+(distanciaviajeInput-distancia1)*tarifa1;
	}else{
		pagodistan+=0;
	}
	return pagodistan;
}
//=====================================================================================================
