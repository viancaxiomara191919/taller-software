/**
 * @file Template.cpp
 * @author VIANCA  XIOMARA  OCROSPOMA  ANCALLI
 * @brief File Exercise 17:
 				El �rea de una elipse se obtiene con la f�rmula ?ab , donde a es el radio menor de la elipse y b es el radio mayor, y su 
 				per�metro se obtiene con la f�rmula ?[4(a+b)2]0.5. Realice un programa en C ++ utilizando estas f�rmulas y calcule el �rea 
		    	y el per�metro de una elipse que tiene un radio menor de 2.5 cm y un radio mayor de 6.4 cm.
 * @version 1.0
 * @date 15.02.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14 	//Constante Pi
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalMenorRad= 0.0;				//Entrada de radio menor 
float terminalMayorRad= 0.0;				//Entarda de radio mayor 

float area = 0.0;
float average = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

float CalculateArea(float MenorRadio, float MayorRad);
float CalculateAverage(float MenorRadio, float MayorRad);
/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\twrite the smallest radius: ";
	cin>>terminalMenorRad;
	cout<<"\tWrite the  radius major: ";
	cin>>terminalMayorRad;
}
//=====================================================================================================
void Calculate(){
	area = CalculateArea(terminalMenorRad,terminalMayorRad);
	average = CalculateAverage(terminalMenorRad,terminalMayorRad);
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe area is: "<<area<<"\r\n";
	cout<<"\tThe average is: "<<average<<"\r\n";
}
//=====================================================================================================
float CalculateArea(float MenorRad, float MayorRad){
	return PI*MenorRad*MayorRad; 								//area = pi*radio menor*radio mayor
}
//=====================================================================================================
float CalculateAverage(float MenorRad, float MayorRad){
	return 2.0*PI*sqrt((pow(MenorRad,2) + pow(MayorRad,2))/2.0);//perimetro hallado
}
//=====================================================================================================

