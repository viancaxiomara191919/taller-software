/**
 * @file Template.cpp
 * @author VIANCA XIOMARA OCROSPOMA ANCALLI
 * @brief File ejercicio 10:
          Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. Al morir dej� el siguiente legado: A Jos� le 
		  dej� 4/3 de lo que le dej� a Carlos. A Carlos le dej� 1/3 de su fortuna. A Marta le dejo la mitad de lo que le dej� 
		  a Jos�. Preparar un algoritmo para darle la suma a repartir e imprima cuanto le toc� a cada uno.
 * @version 1.0
 * @date 15.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

float fortun1=0.0;     
float fortun3=0.0;
float fortun2=0.0;
float fortuna=0.0;
float herencicarlos=0.0;
float herencijose=0.0;
float herencimarta=0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
float fortus(float herencicarlos);
float fortue(float herencijose);
float fortui(float herencimarta); 
float fortu();
void CollectData();
void Run();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
		CollectData();
		Calculate();
		ShowResults();
	 
}
//=====================================================================================================

void CollectData(){
		cout<<"============Insert data============\r\n";
	   cin>> fortuna;
       cin>> herencicarlos;
	   cin >> herencijose; 
 }
//=====================================================================================================
void Calculate(){
	   fortun1 =  fortus(herencicarlos);
       fortun2 =  fortue(herencijose);
       fortun3 =  fortui(herencimarta);
}
 //===================================================================================================== 
  void ShowResults(){
  		cout<<"\r\n========= Show Results ===========\r\n";
  	cout <<"LA HERENCIA DE CARLOS ES: "<<fortun1<<endl;
       cout <<"LA HERENCIA DE JOSE ES: "<<fortun2<<endl;
       cout <<"LA HERENCIA DE MARTA ES: "<<fortun3<<endl;
}
//===================================================================================================== 
	  float fortu(float fortuna ){
	  }
//===================================================================================================== 	  
	  float fortus(float herencicarlos){         //fortuna de carlos= 1/3*fortuna
	  	return fortuna/3;
	  }
//===================================================================================================== 	  
	   float fortue(float herencijose){             //fortuna de jose=4/3*fortuna de carlos
	   	return herencijose=herencicarlos*4/3;
	   }
//===================================================================================================== 	   
	   float fortui(float herencimarta){               //fortuna de marta=fortuna de jose/2
	   	 return herencimarta=herencijose/2;
	   }
	  
