/**
 * @file Template.cpp
 * @author VIANCA XIOMARA OCROSPOMA ANCALLI
 * @brief File Exercise 21:
 				Calcula la velocidad de un auto en Km/h, ingresando la distancia recorrida en metros y el tiempo en minutos
 * @version 1.0
 * @date 28.01.2022
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float distanciaMeters;			//data of the distance in meters is entered
float timeMinutes;				//time data is entered in seconds
		
float Kilometros;				//Data that is used when converting meters to kilometers
float Hours;					//Data that is used when converting seconds to hours
float Speed;	
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

float SecondsToHours(float timeMinutes);
float MetersToKilometros(float distanciaMeters);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main(){
	Run();
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the time in minutes: ";
	cin>>timeMinutes; 
	cout<<"\tWrite the distance in meters: ";
	cin>>distanciaMeters;
}
//=====================================================================================================
void Calculate(){
	Hours = SecondsToHours(timeMinutes);
	Kilometros = MetersToKilometros(distanciaMeters);
	
	Speed = Kilometros/Hours;
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe speed of the car is: "<<Speed<<" Km/h "<<"\r\n";
	
}
//=====================================================================================================
float SecondsToHours(float timeMinutes){
	return timeMinutes/60.0;
}
//=====================================================================================================
float MetersToKilometros(float distanciaMeters){
	return distanciaMeters/1000.0;
}
